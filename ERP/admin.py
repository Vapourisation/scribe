from django.contrib import admin

from .models import *
from TestDjango import admin as custom_admin


# Custom Filters

class SupplierFilter(admin.SimpleListFilter):
    title = 'supplier'
    parameter_name = 'supplier'

    def lookups(self, request, model_admin):
        suppliers = Supplier.objects.all()
        return [(s.name, s.name) for s in suppliers]

    def queryset(self, request, queryset):
        if self.value():
            return queryset.filter(product__supplier=self.value())


class ProductCurrencyInline(admin.TabularInline):
    model = Price
    can_delete = False
    verbose_name_plural = 'Prices'
    fk_name = 'product'


class ProductAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'shortName', 'supplier']
    order = ['id']
    inlines = [
        ProductCurrencyInline,
    ]
    list_filter = [SupplierFilter]
    list_editable = ['name', 'supplier']
    change_list_template = 'admin/TestDjango/change_list.html'


class SupplierProductInline(admin.TabularInline):
    model = Product
    can_delete = False
    verbose_name_plural = 'Products'
    fk_name = 'supplier'
    readonly_fields = ('id', 'name', 'shortName', 'description')


class SupplierAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'location']
    order = ['id']
    inlines = [
        SupplierProductInline,
    ]
    change_list_template = 'admin/TestDjango/change_list.html'


custom_admin.admin_site.register(Currency)
custom_admin.admin_site.register(InternalOrder)
custom_admin.admin_site.register(Product, ProductAdmin)
custom_admin.admin_site.register(Supplier, SupplierAdmin)
