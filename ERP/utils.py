import os
from datetime import *
import csv
import io

from django.shortcuts import render
from django.http import FileResponse, HttpResponse
from django.conf import settings
from django.core import serializers

from reportlab.pdfgen import canvas


def create_pdf(self, items_to_export, *args):
    # TODO: Set up function so it can be used across multiple models and with any number of arguments

    # Create a file-like buffer to receive PDF data.
    buffer = io.BytesIO()

    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas(buffer)

    # Draw things on the PDF. Here's where the PDF generation happens.
    # See the ReportLab documentation for the full list of functionality.
    p.drawString(100, 100, "Hello world.")

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    return FileResponse(buffer, as_attachment=True, filename='hello.pdf')


def create_csv(self, items_to_export, *args):
    pass
