from django.db import models
from django.contrib.auth.models import User

from datetime import datetime

from CRM.const import country


class Product(models.Model):
    name = models.CharField(max_length=60, verbose_name='Product Name')
    shortName = models.CharField(max_length=12, verbose_name='Product Short Name', blank=True)
    description = models.TextField(max_length=500, verbose_name='Product Description', blank=True)
    supplier = models.ForeignKey('Supplier', verbose_name='Product Supplier', on_delete=models.CASCADE,
                                 default=None, null=True)

    def __str__(self):
        return 'Product: ' + self.shortName

    class Meta:
        app_label = 'ERP'
        verbose_name = 'Product'
        verbose_name_plural = 'Products'


class Currency(models.Model):
    description = models.CharField(max_length=120, blank=True, verbose_name='Currency description')
    shortName = models.CharField(max_length=3, verbose_name='Displayed name beforeimport/after the price')
    symbol = models.CharField(max_length=1, verbose_name='Currency symbol', default='£')
    rounding = models.DecimalField(max_digits=5, decimal_places=2, verbose_name='Rounding',
                                   help_text='Should be a decimal such as .00, .10, .20', blank=True, null=True)

    def __str__(self):
        return 'Currency: ' + self.shortName

    class Meta:
        app_label = 'ERP'
        verbose_name = 'Currency'
        verbose_name_plural = 'Currency'


class Supplier(models.Model):
    name = models.CharField(max_length=50)
    location = models.CharField(choices=[(x[0], x[3]) for x in country.COUNTRIES], blank=True, max_length=30)

    def __str__(self):
        return 'Supplier: ' + self.name

    class Meta:
        app_label = 'ERP'
        verbose_name = 'Supplier'
        verbose_name_plural = 'Suppliers'


class Price(models.Model):
    product = models.ForeignKey(Product, verbose_name='Product', on_delete=models.CASCADE)
    currency = models.ForeignKey(Currency, blank=False, null=False, verbose_name='Currency', on_delete=models.CASCADE)
    price = models.DecimalField(max_digits=17, decimal_places=2, verbose_name='Price Per Unit')
    validfrom = models.DateField(verbose_name='Valid from', blank=True, null=True)
    validuntil = models.DateField(verbose_name='Valid until', blank=True, null=True)

    def is_valid_from_criteria_fulfilled(self, date):
        if self.validfrom == None:
            return True;
        elif (self.validfrom - date).days <= 0:
            return True;
        else:
            return False;

    def is_valid_until_criteria_fulfilled(self, date):
        if self.validuntil == None:
            return True
        elif (date - self.validuntil).days <= 0:
            return True
        else:
            return False

    def is_currency_criteria_fulfilled(self, currency):
        if self.currency == currency:
            return True
        else:
            return False

    class Meta:
        app_label = 'ERP'
        verbose_name = 'Price'
        verbose_name_plural = 'Prices'


class InternalOrder(models.Model):
    products = models.ForeignKey(Product, on_delete=models.PROTECT, null=False, blank=False)
    customer = models.ForeignKey(User, default=1, on_delete=models.PROTECT)
    order_date = models.DateTimeField(default=datetime.now)
    # TODO: Add in an authorised check and create field for it

    def __str__(self):
        return 'Internal Order: #{}'.format(self.id)

    class Meta:
        app_label = 'ERP'
        verbose_name = 'Internal Order'
        verbose_name_plural = 'Internal Orders'
