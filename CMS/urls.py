from django.conf import settings
from django.contrib import admin
from django.urls import path
from . import views


urlpatterns = [
    path('', views.index, name='home'),
    path('<slug:slug>/', views.PageDetailView.as_view(template_name='page.html')),
    path('<slug:slug>/<slug:child>/', views.child_page)
]