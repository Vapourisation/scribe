from django.contrib import admin
from django.urls import path
from django.template.response import TemplateResponse
from .models import *

from TestDjango import admin as custom_admin


class SubpageAdmin(admin.ModelAdmin):
    model = Subpage
    readonly_fields = ['url', 'level']
    inlines = [
        # PageInline
    ]
    change_list_template = 'admin/TestDjango/change_list.html'

    def get_urls(self):
        urls = super(SubpageAdmin, self).get_urls()
        my_urls = [
            path('admin/CMS/pages/', self.admin_site.admin_view(self.my_view)),
        ]
        return urls + my_urls

    def my_view(self, request):
        # ...
        context = dict(
            self.admin_site.each_context(request),
        )
        return TemplateResponse(request, "admin/pages.html", context)


class PageAdmin(admin.ModelAdmin):
    model = Page
    readonly_fields = ['url', 'level']
    list_display = ['title', 'slug', 'creator', 'live']
    inlines = [
        # PageInline
    ]
    change_list_template = 'admin/TestDjango/change_list.html'

    def get_urls(self):
        urls = super(PageAdmin, self).get_urls()
        my_urls = [
            path('admin/CMS/pages/', self.admin_site.admin_view(self.my_view)),
        ]
        return urls + my_urls

    def my_view(self, request):
        # ...
        context = dict(
            self.admin_site.each_context(request),
        )
        return TemplateResponse(request, "admin/pages.html", context)


custom_admin.admin_site.register(Page, PageAdmin)
custom_admin.admin_site.register(Subpage, SubpageAdmin)
