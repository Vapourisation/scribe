import uuid

from django.core.cache import cache
from django.db import models
from django.db.models import Q
from django.conf import settings
from ckeditor.fields import RichTextField

from .queries import PageQuerySet
from . import fields as CMSFields


class BasePageManager(models.Manager):
    def get_queryset(self):
        return super(BasePageManager, self).get_queryset().order_by('title')

    # def filter(self, params):
    #     return super(BasePageManager, self).get_queryset().filter(params)


PageManager = BasePageManager


class AbstractPage(models.Model):

    class Meta:
        abstract = True


class Page(AbstractPage):
    objects = BasePageManager()

    title = models.CharField(
        verbose_name='title',
        max_length=255,
        help_text='The title of the page as you\'d like it to be seen'
    )
    url = models.SlugField(
        verbose_name='URL',
        allow_unicode=True,
        max_length=255,
        unique=True,
        null=True,
        help_text='The URL for the page. This will be auto-generated once you save from the slug, '
                  'and if a parent exists, that page slug too'
    )
    slug = models.SlugField(
        verbose_name='slug',
        allow_unicode=True,
        max_length=255,
        help_text='The name of the page as it will appear in the URLs e.g. http://your-domain.com/blog/{slug}',
        unique=True
    )
    live = models.BooleanField(
        verbose_name='live',
        default=False,
    )
    creator = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        verbose_name='creator',
        null=True,
        blank=True,
        editable=True,
        on_delete=models.SET_NULL,
    )
    seo_title = models.CharField(
        verbose_name='SEO page title',
        max_length=255,
        blank=True,
        help_text='An optional title for the page that will appear in the meta tags and browser window'
    )
    seo_description = models.TextField(
        verbose_name='SEO description',
        blank=True,
        help_text='Optional page description that will only appear in the meta tags for sharing and SEO purposes'
    )
    content = RichTextField(
        default='Test'
    )
    is_parent = models.BooleanField(
        default=False,
        verbose_name='Set this page to be a parent to set other pages as its child'
    )
    is_home = models.BooleanField(
        default=False,
        verbose_name='Set this page as the sites homepage',
    )
    children = models.ForeignKey(
        'Subpage',
        default=False,
        blank=True,
        null=True,
        on_delete=models.CASCADE
    )
    level = models.IntegerField(
        default=0,
        blank=False,
        null=False
    )

    def __str__(self):
        return self.title

    def get_queryset(self):
        return self.objects.get_queryset()

    def has_children(self):
        if self.is_parent:
            return True

    # def get_children(self):
    #     return self.objects.filter(f'parent={self.title}')

    def save(self, *args, **kwargs):
        self.url = self.slug
        cache.delete('top_level_navigation')
        cache.delete('secondary_navigation')
        super().save(*args, **kwargs)


class Subpage(Page):
    objects = BasePageManager()

    is_child = models.BooleanField(
        default=True,
        verbose_name='Set this page as a child of another page'
    )
    parent = models.ForeignKey(
        Page,
        default=False,
        blank=False,
        null=False,
        on_delete=models.CASCADE,
        related_name='Parent'
    )

    def __str__(self):
        return self.title

    def get_queryset(self):
        return self.objects.get_queryset()

    def save(self, *args, **kwargs):
        self.parent.is_parent = True
        self.level = 1
        cache.delete('top_level_navigation')
        cache.delete('secondary_navigation')
        super().save(*args, **kwargs)
