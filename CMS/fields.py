from django.db import models
from django.conf import settings
import os
from uuid import uuid4
from django.utils.deconstruct import deconstructible


@deconstructible
class UploadToPathAndRename(object):

    def __init__(self, path):
        self.sub_path = path

    def __call__(self, instance, filename):
        ext = filename.split('.')[-1]
        # get filename
        if instance.pk:
            filename = '{}.{}'.format(instance.pk, ext)
        else:
            # set filename as random string
            filename = '{}.{}'.format(uuid4().hex, ext)
        # return the whole path to the file
        return os.path.join(self.sub_path, filename)


class HeadingField(models.Model):
    types = (
        ('h1', 'h1'),
        ('h2', 'h2'),
        ('h3', 'h3'),
        ('h4', 'h4'),
        ('h5', 'h5'),
        ('h6', 'h6')
    )
    style = models.CharField(choices=types, max_length=2, verbose_name='Style')
    content = models.CharField(max_length=120, verbose_name='Content')
    css_class = models.CharField(max_length=30, verbose_name='Class', blank=True, null=True)


class TextField(models.Model):
    content = models.TextField(verbose_name='Content')


class ImageField(models.Model):
    content = models.ImageField(
        verbose_name='Image',
        upload_to=UploadToPathAndRename(os.path.join(settings.MEDIA_ROOT, '/cms/pages/images/'))
    )


class VideoField(models.Model):
    content = models.FileField(
        verbose_name='Video',
        upload_to=UploadToPathAndRename(os.path.join(settings.MEDIA_ROOT, '/cms/page/videos'))
    )
