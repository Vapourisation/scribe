from django.db.models import Q
from django.db.models.functions import Length, Substr
from django.db.models.query import BaseIterable

from treebeard.mp_tree import MP_NodeQuerySet


class TreeQuerySet(MP_NodeQuerySet):
    pass


class PageQuerySet(TreeQuerySet):
    pass
