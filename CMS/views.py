import uuid

from django.http import HttpResponseRedirect
from django.shortcuts import render, get_object_or_404
from django.views import generic
from django.core.cache import cache

from . import models


def index(request):

    if cache.get('home') is not None:
        page = cache.get('home')
        return render(request, 'page.html', {'page': page})
    else:
        pages = models.Page.objects.filter(is_home=True, live=True)
        for page in pages:
            page = get_object_or_404(page)
            cache.set('home', page)
            return render(request, 'page.html', {'page': page})
    return render(request, 'index.html')


def home(request):
    pages = models.Page.objects.filter(is_home=True, live=True)
    return render(request, 'index.html', {'page': pages})


def subpage(request, sub):
    if cache.get(sub) is not None:
        page = cache.get(sub)
        return render(request, 'page.html', {'page': page})
    else:
        pages = models.Page.objects.filter(slug=sub, live=True)
        for page in pages:
            page = get_object_or_404(page)
            cache.set(sub, page)
            return render(request, 'page.html', {'page': page})


def child_page(request, slug, child):
    if cache.get(child) is not None:
        page = cache.get(child)
        return render(request, 'page.html', {'page': page})
    else:
        pages = models.Page.objects.filter(slug=child, live=True)
        page = get_object_or_404(pages)
        cache.set(child, page)
        return render(request, 'page.html', {'page': page})


class IndexPageView(generic.ListView):
    model = models.Page
    template_name = 'project/index.html'

    def get_queryset(self):
        return models.Page.objects.all()


class PageDetailView(generic.DetailView):

    def get_queryset(self):
        return models.Page.objects.filter(slug=self.kwargs['slug'])


class ChildPageDetailView(generic.DetailView):

    def get_queryset(self):
        return models.Page.objects.filter(slug=self.kwargs['child'])
