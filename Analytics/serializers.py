from rest_framework import serializers
from .models import BaseAnalytics


class AnalyticsSerializer(serializers.ModelSerializer):
    class Meta:
        model = BaseAnalytics
        fields = ("name", "data")
