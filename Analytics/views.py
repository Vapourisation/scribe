from django.shortcuts import render
from rest_framework import generics, status
from rest_framework.decorators import api_view
from rest_framework.response import Response
from .models import BaseAnalytics
from .serializers import AnalyticsSerializer


class ListAnalyticsView(generics.ListAPIView):
    queryset = BaseAnalytics.objects.all()
    serializer_class = AnalyticsSerializer


@api_view(['POST', 'PATCH', 'UPDATE'])
def analytics_register(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = BaseAnalytics.objects.all()
        serializer = AnalyticsSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        serializer = AnalyticsSerializer(data=request.data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)
