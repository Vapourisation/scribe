from django.db import models
from django.contrib.postgres.fields import JSONField

analytics_type_choices = [
    (1, 'Page View'),
    (2, 'SMS Sent'),
    (3, 'SMS Received'),
    (4, 'Email Sent'),
    (5, 'Email Received'),
    (6, 'Product Added To Basket'),
    (7, 'Product Removed From Basket'),
    (8, 'Order Finalised'),
    (9, 'Order Started'),
    (10, 'Order Abandoned'),
    (11, 'Login'),
    (12, 'Logout'),
    (13, 'Account Create'),
    (14, 'Account Change'),
    (15, 'Account Remove')
]


class BaseAnalytics(models.Model):
    name = models.CharField(max_length=30)
    type = models.CharField(max_length=20, choices=[(x[0], x[1]) for x in analytics_type_choices])
    data = JSONField()

    change_list_template = 'admin/TestDjango/change_list.html'

    @classmethod
    def create(cls, name, data):
        analytics = cls(name=name, data=data)
        return analytics

    def save(self, *args, **kwargs):
        if not isinstance(self.name, str):
            return
        else:
            super(BaseAnalytics, self).save(using='analytics_db', *args, **kwargs)

    class Meta:
        app_label = 'analytics'
        verbose_name = 'Analytics'
        db_table = 'Analytics_BaseAnalytics'
