class AnalyticsRouter(object):
    """
    A router to control all database operations on models in the
    Analytics application.
    """
    def db_for_read(self, model, **hints):
        """
        Attempts to read analytics models go to analytics_db.
        """
        if model._meta.app_label == 'analytics':
            return 'analytics_db'
        return None

    def db_for_write(self, model, **hints):
        """
        Attempts to write analytics models go to analytics_db.
        """
        if model._meta.app_label == 'analytics':
            return 'analytics_db'
        return None

    def allow_relation(self, obj1, obj2, **hints):
        """
        Allow relations if a model in the analytics app is involved.
        """
        if obj1._meta.app_label == 'analytics' or \
           obj2._meta.app_label == 'analytics':
           return True
        return None

    def allow_migrate(self, db, app_label, model_name=None, **hints):
        """
        Make sure the analytics app only appears in the 'analytics_db'
        database.
        """
        if app_label == 'analytics':
            return db == 'analytics_db'
        return None
