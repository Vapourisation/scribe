import os
from datetime import *
import csv
import io

from django.shortcuts import render
from django.http import FileResponse, HttpResponse
from django.conf import settings
from django.core import serializers
from twilio.rest import Client

from reportlab.pdfgen import canvas
from reportlab.lib.pagesizes import letter, A4
from reportlab.lib.units import cm


def create_pdf(self):
    # TODO: Set up function so it can be used across multiple models and with any number of arguments

    # FileResponse sets the Content-Disposition header so that browsers
    # present the option to save the file.
    response = HttpResponse(content_type='application/pdf')
    response['Content-Disposition'] = 'attachment; filename="order.pdf"'

    # Create the PDF object, using the buffer as its "file."
    p = canvas.Canvas('order.pdf', pagesize=letter)

    width, height = letter

    p.setFont('Helvetica', 14)
    p.setStrokeColorRGB(0.2, 0.5, 0.3)
    p.setFillColorRGB(1, 0, 1)
    p.line(0, 0, 1*cm, 0)
    p.drawString(100, 100, str(self.id))

    # Close the PDF object cleanly, and we're done.
    p.showPage()
    p.save()

    return response


def create_csv(self, item_to_export, *args):
    pass
