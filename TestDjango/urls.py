from django.conf import settings
from django.conf.urls import url
from django.conf.urls.static import static
from django.contrib.auth import views as auth_views
from django.urls import include, path, re_path
from django.views.generic import TemplateView

from . import views
from . import admin
from catalog import views as catalog_views
from UserProfiles import views as user_profile_views

import debug_toolbar

urlpatterns = []

if settings.DEBUG:

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]


urlpatterns += [
    path('backend/', admin.admin_site.urls),
    path(
        'backend/password_reset/',
        auth_views.PasswordResetView.as_view(),
        name='admin_password_reset',
    ),
    path(
        'backend/password_reset/done/',
        auth_views.PasswordResetDoneView.as_view(),
        name='password_reset_done',
    ),
    path(
        'reset/<uidb64>/<token>/',
        auth_views.PasswordResetConfirmView.as_view(),
        name='password_reset_confirm',
    ),
    path(
        'reset/done/',
        auth_views.PasswordResetCompleteView.as_view(),
        name='password_reset_complete',
    ),
    path('', include('CMS.urls')),
    path('signup', views.signup, name='signup'),
    path('login', views.userLogin, name='login'),
    path('logout', views.userLogout, name='logout'),
    path('profile/<str:user_name>', user_profile_views.profile, name='user-profile'),
    path('api_example', include('API.urls')),
    path('api/analytics', views.analytics, name='analytics'),
    path('api/public/', catalog_views.public),
    path('api/private/', catalog_views.private)
] + static(settings.STATIC_URL, document_root=settings.STATIC_ROOT) + static(settings.MEDIA_URL,
                                                                           document_root=settings.MEDIA_ROOT)
