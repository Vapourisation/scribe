import {Event} from "./Event.js";

const dateTime = Event.getDate();

class PageView extends Event {

    constructor() {
        super('Page view', 'PageView', dateTime)
    };

    register(url, token) {
        const xhr = new XMLHttpRequest();

        xhr.onreadystatechange = () => {
            if (xhr.readyState === 4 && xhr.status === 200) {
                console.log('Ready');
            }
        };

        xhr.onload = () => {

            if (xhr.status >= 200 && xhr.status < 300) {
                console.log('success!');
            } else {
                console.log('The request failed!');
            }

        };

        const data = JSON.stringify({
            data : {
                name: this.name,
                type: this.type,
                timestamp: dateTime
            }
        });

        xhr.open('POST', url);
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.setRequestHeader("X-CSRFToken", token );
        xhr.send(data);
    };
}

export {PageView}