(function() {

    // Modal control
    const modalButtons = document.querySelectorAll( '[data-modal]');

    modalButtons.forEach( ( button ) => {
        button.addEventListener( 'click', () => {
            const modal = document.getElementById( button.dataset.modal );
            modal.classList.toggle( 'is-active' );
            document.querySelector( 'html' ).classList.toggle( 'is-modal-open' );
        })
    })

    // Animation control
    const isInViewport = ( elem ) => {
        const bounding = elem.getBoundingClientRect();
        return (
                bounding.top >= 0 &&
                bounding.left >= 0 &&
                bounding.bottom <= (window.innerHeight || document.documentElement.clientHeight) &&
                bounding.right <= (window.innerWidth || document.documentElement.clientWidth)
            );
    };

    const animateIn = document.querySelectorAll( '[data-animate-in]' );

    window.addEventListener( 'load', () => {
        animateIn.forEach((elem) => {
            if (isInViewport(elem)) {
                elem.classList.add(elem.dataset.animateIn);
            }
        })
    });

    window.addEventListener( 'scroll', () => {
        animateIn.forEach((elem) => {
            if (isInViewport(elem)) {
                elem.classList.add(elem.dataset.animateIn);
            }
        })
    });

})();

const scrollOnClick = () => {
    const scroller = document.querySelectorAll( '[data-scroll]' );

    scroller.forEach( ( scroll ) => {
        const location = document.getElementById( scroll.dataset.scroll );

        scroll.addEventListener( 'click', () => {
           location.scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"})
        });
    });
};

const assignToggleEffects = () => {
    const toggler = document.querySelectorAll( '[data-toggle]');

    toggler.forEach( ( e ) => {
        const target = document.querySelector( `.${e.getAttribute( 'data-toggle' )}` );
        const targetChildren = target.querySelectorAll( 'li' );
        const icon = e.querySelector( 'i' );
        let blurContent = '';
        if( e.dataset.location === 'nav' ){
            blurContent = [...document.getElementsByTagName( 'section' )];
        }

        e.addEventListener( 'click', ( e ) => {
            icon.classList.toggle( 'spin' );
            target.classList.toggle( 'show' );
            targetChildren.forEach( ( t ) => {
                t.classList.toggle( 'show' );
            });
            if( blurContent !== '' ) {
                blurContent.forEach( (b) => {
                    b.classList.toggle('blur');
                })
            }
        })
    });
};

window.onload = () => {
    assignToggleEffects();
    scrollOnClick();
};
