class Event {
    constructor(name, type, timestamp) {
        this.name = name;
        this.type = type;
        this.timestamp = timestamp;
    }

    static getDate() {
        let date = new Date();
        let day = date.getDate();
        let month = date.getMonth() + 1; //January is 0!
        let hours = date.getHours();
        let minutes = date.getMinutes();
        let seconds = date.getSeconds();
        const year = date.getFullYear();

        if (day < 10) {
            day = '0' + day
        }

        if (month < 10) {
            month = '0' + month
        }

        return `${day}/${month}/${year} - ${hours}:${minutes}:${seconds}`;
    }
}

export {Event};