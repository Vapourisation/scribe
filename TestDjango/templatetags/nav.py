import uuid

from django import template
from django.core.cache import cache

from CMS import models as cms_models

register = template.Library()


@register.inclusion_tag('menus/menu.html')
def nav():

    if cache.get('top_level_navigation') is not None and cache.get('secondary_navigation') is not None:
        top_level_navigation = cache.get('top_level_navigation')
        secondary_navigation = cache.get('secondary_navigation')
        return {'top_level_navigation': top_level_navigation, 'secondary_navigation': secondary_navigation}
    elif cache.get('top_level_navigation') is None and cache.get('secondary_navigation') is None:
        top_level_navigation = list(cms_models.Page.objects.values_list('url', 'title'))
        secondary_navigation = list(cms_models.Subpage.objects.values_list('url', 'title', 'parent'))
        cache.set('top_level_navigation', top_level_navigation)
        cache.set('secondary_navigation', secondary_navigation)
        return {'top_level_navigation': top_level_navigation, 'secondary_navigation': secondary_navigation}
