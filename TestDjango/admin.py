from django.contrib import admin


class MyAdminSite(admin.AdminSite):
    site_header = 'Scribe Administration'
    index_template = 'admin/TestDjango/index.html'
    app_index_template = 'admin/TestDjango/index.html'
    login_template = 'admin/TestDjango/login.html'
    logout_template = 'admin/TestDjango/logout.html'
    change_form_template = 'admin/TestDjango/change_form.html'
    change_list_template = 'admin/TestDjango/change_list.html'
    object_history_template = 'admin/TestDjango/object_history.html'
    popup_response_template = 'admin/TestDjango/popup_response.html'


admin_site = MyAdminSite(name='myadmin')
