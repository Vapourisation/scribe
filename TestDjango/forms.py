from django import forms
from . import models as django_models
from UserProfiles import models as user_models
from CRM import models as crm_models


class LoginForm(forms.ModelForm):
    class Meta:
        model = user_models.User
        fields = ['username', 'password']
