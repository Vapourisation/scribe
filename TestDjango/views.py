import json

from django.shortcuts import render, redirect
from django.contrib import messages
from django.contrib.auth import authenticate, logout, login
from django.contrib.auth.forms import UserCreationForm
from django.http import HttpResponseRedirect, JsonResponse

from ipware import get_client_ip

from Analytics.models import *
from CMS import models as cms_models
from ERP.models import *
from Accounting.models import *


def index(request):
    pages = cms_models.Page.objects.all()
    products = Product.objects.all()
    accounts = Account.objects.all()

    return render(request, 'index.html', {
        'page_title': 'CCRMS Homepage',
        'pages': pages,
        'products': products,
        'accounts': accounts
    })


def userLogout(request):
    messages.success(request, 'Successfully logged out')
    logout(request)
    return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def userLogin(request):
    username = request.POST['username']
    password = request.POST['password']
    user = authenticate(request, username=username, password=password)
    if user is not None:
        messages.success(request, 'Successfully logged in')
        login(request, user)
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
    else:
        messages.error(request, 'Unable to login. Please check the errors on the form')
        return HttpResponseRedirect(request.META.get('HTTP_REFERER'))


def signup(request):
    if request.method == 'POST':
        form = UserCreationForm(request.POST)
        if form.is_valid():
            form.save()
            username = form.cleaned_data.get('username')
            raw_password = form.cleaned_data.get('password1')
            user = authenticate(username=username, password=raw_password)
            login(request, user)
            return redirect('home')
    else:
        form = UserCreationForm()
    return render(request, 'profiles/signup.html', {'form': form})


def analytics(request):
    if request.method == 'POST':
        sent_data = request.body
        decoded_data = sent_data.decode('utf8').replace("'", '"')
        data = json.loads(decoded_data)
        json_data = json.dumps(data, indent=4, sort_keys=True)

        client_ip, is_routable = get_client_ip(request)

        if client_ip is None:
            ip = 'Unknown'
        else:
            if is_routable:
                ip = client_ip
            else:
                ip = 'Private IP'

        analytics_instance = BaseAnalytics.create(data['data']['name'], data['data'])
        # analytics_instance.save()

        data = {
            'status': '',
            'ip': ip,
            'message': 'Success',
            'send_data': json_data
        }
        return JsonResponse(data)
