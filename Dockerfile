# Use an official Python runtime as a parent image
FROM python:3.8
LABEL maintainer="tpegler92@gmail.com"

# Set environment varibles
ENV PYTHONUNBUFFERED 1
ENV DJANGO_ENV dev
ENV POSTGRES_DB scribe
ENV POSTGRES_USER scribbler
ENV POSTGRES_PASSWORD scribbled
ENV GOOGLE_MAPS_KEY AIzaSyCMd0LzDZ2zSy7Zf4SXUKKVlgsw7RNqiwY
ENV TWILIO_SID AC9e4cb326d001a798e7ab5ac193c18fb5
ENV TWILIO_TOKEN e9ee188182c63755b98b58fa75e12994
ENV TWILIO_NUMBER +441183247032


COPY ./requirements.txt /code/requirements.txt
RUN pip install --upgrade pip
# Install any needed packages specified in requirements.txt
RUN pip install -r /code/requirements.txt
RUN pip install daphne

# Copy the current directory contents into the container at /code/
COPY . /code/
# Set the working directory to /code/
WORKDIR /code/

EXPOSE 8001
CMD exec daphne TestDjango.asgi:application --bind 0.0.0.0 -p 8001
