from CRM.messaging import Email, SMSMessage


def send_sms_message(message_id, sender, recipient):
    sms = SMSMessage(sender=sender, recipient=recipient)
    sms.send(message_id=message_id)


def send_email(sender, recipient, template, email_header):
    email = Email(sender=sender, recipient=recipient, template_name=template, email_header=email_header)
    email.send()
