from django.contrib import admin
from TestDjango import utils
from CRM.models import *

from TestDjango import admin as custom_admin


class CustomerOrderInline(admin.StackedInline):
    model = Order
    can_delete = False
    verbose_name_plural = 'Orders'
    fk_name = 'customer'
    readonly_fields = ('products', 'orderDate', 'account')
    extra = 1


class CustomerAdmin(admin.ModelAdmin):
    list_display = ['user', 'profile', 'telephone', 'mobile', 'location', 'orders']
    order = ['orderDate']
    inlines = [
        CustomerOrderInline
    ]
    search_fields = ['user', 'telephone', 'orders']
    list_filter = ['created_at']
    change_list_template = 'admin/TestDjango/change_list.html'


class OrderAdmin(admin.ModelAdmin):
    actions = ['utils.create_pdf(Order)']
    change_list_template = 'admin/TestDjango/change_list.html'


class MessageAdmin(admin.ModelAdmin):
    def render_change_form(self, request, context, *args, **kwargs):
        # here we define a custom template
        self.change_form_template = 'admin/CRM/change_form_help_text.html'
        extra = {
            'help_text': "Try to keep the message amount to a minimum so searching is faster and there are no "
                         "duplicates. Names have to be unique. If you find yourself needing the same messages again "
                         "you should consider re-using the older message and what you really need to send."
        }

        context.update(extra)
        return super(MessageAdmin, self).render_change_form(request, context, *args, **kwargs)


custom_admin.admin_site.register(Customer, CustomerAdmin)
custom_admin.admin_site.register(Order, OrderAdmin)
custom_admin.admin_site.register(Message, MessageAdmin)

