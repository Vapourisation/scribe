from django.shortcuts import render
from rest_framework.views import APIView

from CRM.utils import send_email, send_sms_message


class SendEmail(APIView):
    def post(self, request):
        send_email(*request.data)


send_mail = SendEmail.as_view()


class SendSms(APIView):
    def post(self, request):
        send_sms_message(*request.data)


send_sms = SendSms.as_view()
