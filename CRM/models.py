from django.core.validators import RegexValidator
from django.db import models
from django.contrib.auth.models import User
from django.utils.timezone import datetime

from UserProfiles.models import Profile
from Accounting.models import Account
from ERP import models as erp_models

from .const import country

# Create your models here.

phone_regex = RegexValidator(regex=r'^\+?1?\d{9,15}$',
                             message="Phone number must be entered in the format: '+999999999'.")


class Customer(models.Model):
    user = models.OneToOneField(User, blank=True, on_delete=models.CASCADE)
    profile = models.OneToOneField(Profile, blank=True, verbose_name='Customer Profile', on_delete=models.CASCADE,
                                   null=True)
    telephone = models.CharField(validators=[phone_regex], max_length=17, blank=True, verbose_name='Landline Number',
                                 default='+123456789', null=True)
    mobile = models.CharField(validators=[phone_regex], max_length=17, blank=True, verbose_name='Mobile Number',
                              default='+123456789', null=True)
    orders = models.ForeignKey('Order', on_delete=models.CASCADE, blank=True, verbose_name='Customer Orders',
                               related_name='Order', null=True)
    location = models.CharField(choices=[(x[0], x[3]) for x in country.COUNTRIES], blank=True,
                                verbose_name='Customer Location', max_length=30, null=True)
    created_at = models.DateTimeField(default=datetime.now, blank=False,
                                      verbose_name='Time the customer was first created')

    def __str__(self):
        return f'Customer: {self.user.username}'

    class Meta:
        app_label = 'CRM'
        verbose_name = 'Customer'
        verbose_name_plural = 'Customers'


class Order(models.Model):
    customer = models.ForeignKey(Customer, on_delete=models.CASCADE)
    products = models.ManyToManyField(erp_models.Product, verbose_name='The products in the order')
    orderDate = models.DateTimeField(default=datetime.now, verbose_name='The date the order was placed')
    account = models.ForeignKey(Account, on_delete=models.CASCADE, blank=True, null=True)

    change_list_template = 'admin/TestDjango/change_list.html'

    def __str__(self):
        return f'Order: #{str(self.id)}'

    class Meta:
        app_label = 'CRM'
        verbose_name = 'Order'
        verbose_name_plural = 'Orders'


class Message(models.Model):
    name = models.CharField(max_length=250, null=False, blank=False,
                            help_text='A unique name for the message', unique=True
                            )
    message = models.TextField(null=False, blank=False,
                                help_text='Specify the content of the text message that will be sent to the contact'
                                )
    user = models.ForeignKey(User, on_delete=models.SET_DEFAULT, default=1)
