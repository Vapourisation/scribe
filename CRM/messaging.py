import json

from django.conf import settings
from django.core.mail import send_mail
from django.template.loader import render_to_string
from twilio.rest import Client

from Analytics.models import BaseAnalytics
from CRM.models import Message


class SMSMessage:
    def __init__(self, sender, recipient):
        self.sender = sender
        self.recipient = recipient

    def send(self, message_id):
        message_body = Message.objects.get(id=message_id)
        account_sid = settings.API_KEYS['twilio']['sid']
        account_token = settings.API_KEYS['twilio']['token']
        account_number = settings.API_KEYS['twilio']['number']
        client = Client(account_sid, account_token)

        message = client.messages.create(
            body=message_body.message,
            from_=account_number,
            to=self.recipient
        )
        analytics_data = {
            'Recipient': self.recipient,
            'Message': message_body
        }
        analytics = BaseAnalytics.objects.create(name='SMS Sent', type=2, data=json.dumps(analytics_data))
        analytics.save()
        return message.status


class Email:
    def __init__(self, sender, recipient, template_name, email_header):
        self.sender = sender
        self.recipient = recipient
        self.template = template_name
        self.email_header = email_header

    def render_template(self, template_name):
        context = {
            'recipient': self.recipient,
            'sender': self.sender,
        }
        msg_plain = render_to_string(f'{settings.BASE_DIR}/TestDjango/templates/{template_name}.txt', context)
        msg_html = render_to_string(f'{settings.BASE_DIR}/TestDjango/templates/{template_name}.html', context)

        return msg_plain, msg_html

    def send(self):
        message_plain, message_html = self.render_template(self.template)

        send_mail(
            self.email_header,
            message_plain,
            self.sender,
            [self.recipient],
            html_message=message_html,
        )

        analytics_data = {
            'Recipient': self.recipient,
            'Sender': self.sender,
            'Message': message_plain
        }

        analytics = BaseAnalytics.objects.create(name='Email Sent', type=4, data=json.dumps(analytics_data))
        analytics.save()
        return f'Email sent to {self.recipient}'
