from django.contrib import admin
from .models import *

from CRM.models import Order
from TestDjango import admin as custom_admin


class AccountOrderInline(admin.TabularInline):
    model = Order
    can_delete = False
    verbose_name_plural = 'Orders'
    fk_name = 'account'
    readonly_fields = ('customer', 'products', 'orderDate')


class AccountAdmin(admin.ModelAdmin):
    list_display = ['id', 'name', 'exec']
    order = ['id']
    inlines = [
        AccountOrderInline,
    ]
    change_list_template = 'admin/TestDjango/change_list.html'


custom_admin.admin_site.register(Account, AccountAdmin)
