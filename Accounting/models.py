from django.contrib.auth.models import User
from django.db import models


class Account(models.Model):
    exec = models.ForeignKey(User, blank=True, verbose_name='Exec', on_delete=models.CASCADE, default=0)
    name = models.CharField(max_length=30, verbose_name='Account Name',
                            help_text='An easy name to remember the account by e.g. Walmart - Tech Dept.')

    def __str__(self):
        return 'Account: ' + self.name

    class Meta:
        app_label = 'Accounting'
        verbose_name = 'Account'
        verbose_name_plural = 'Accounts'
