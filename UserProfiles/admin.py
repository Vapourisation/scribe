from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from django.contrib.auth.models import User

from .models import Profile
from TestDjango import admin as custom_admin


class ProfileInline(admin.StackedInline):
    model = Profile
    can_delete = False
    verbose_name_plural = 'Profile'
    fk_name = 'user'
    extra = 1


class CustomUserAdmin(UserAdmin):
    inlines = (ProfileInline, )
    change_list_template = 'admin/TestDjango/change_list.html'

    def get_inline_instances(self, request, obj=None):
        if not obj:
            return list()
        return super(CustomUserAdmin, self).get_inline_instances(request, obj)


custom_admin.admin_site.register(User, CustomUserAdmin)
