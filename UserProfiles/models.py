from CRM.const.country import *
from django.contrib.auth.models import User, AbstractUser
from django.db import models
from django.db.models.signals import post_save
from django.dispatch import receiver


class Profile(models.Model):
    PUBLIC = 1
    MEMBER = 2
    ADMIN = 3
    ROLE_CHOICES = (
        (PUBLIC, 'Public'),
        (MEMBER, 'Member'),
        (ADMIN, 'Admin'),
    )
    user = models.OneToOneField(User, verbose_name="User", on_delete=models.CASCADE)
    bio = models.TextField(max_length=500, verbose_name="A short statement about you", blank=True)
    location = models.CharField(max_length=2, choices=[(x[0], x[3]) for x in COUNTRIES], blank=True)
    birth_date = models.DateField(null=True, blank=True)
    image = models.ImageField(upload_to='media/users/profile', default='icon.png')
    role = models.PositiveSmallIntegerField(choices=ROLE_CHOICES, null=True, blank=True, default=PUBLIC)

    def __str__(self):
        return self.user.username + '\'s profile'

    class Meta:
        verbose_name = 'User Profile'
        verbose_name_plural = 'User Profiles'


@receiver(post_save, sender=User)
def create_user_profile(sender, instance, created, **kwargs):
    if created:
        Profile.objects.create(user=instance)


@receiver(post_save, sender=User)
def save_user_profile(sender, instance, **kwargs):
    instance.profile.save()
