from django.contrib.auth.decorators import login_required
from django.db import transaction
from django.contrib import messages
from django.conf import settings
from django.shortcuts import render, redirect
from django.contrib.auth.models import User

from UserProfiles.forms import ProfileForm, UserForm
from UserProfiles.models import Profile


@login_required
@transaction.atomic
def update_profile(request):
    if request.method == 'POST':
        user_form = UserForm(request.POST, instance=request.user)
        profile_form = ProfileForm(request.POST, instance=request.user.profile)
        if user_form.is_valid() and profile_form.is_valid():
            user_form.save()
            profile_form.save()
            messages.success(request, 'Your profile was successfully updated!')
            return redirect('settings:profile')
        else:
            messages.error(request, 'Please correct the error below.')
    else:
        user_form = UserForm(instance=request.user)
        profile_form = ProfileForm(instance=request.user.profile)
    return render(request, 'profiles/profile.html', {
        'user_form': user_form,
        'profile_form': profile_form
    })


@login_required
def profile(request, user_name):
    user_profile = Profile.objects.get(user__username=user_name)
    maps_key = settings.API_KEYS['google_maps']
    data = {
        'profile': user_profile,
        'user': user_profile.user,
        'maps_key': maps_key
    }

    response = render(request, template_name='profiles/profile.html', context=data)

    return response
